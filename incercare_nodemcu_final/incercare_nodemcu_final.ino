#include <ESP8266WiFi.h>
#include <FirebaseArduino.h>
#include <ArduinoJson.h>
#include <ESP8266HTTPClient.h>
#include <SoftwareSerial.h>

SoftwareSerial nodemcu(D6, D5);

#define ON_Board_LED 2  

const char* ssid = "VodafoneMobileWiFi-676100"; 
const char* password = "fl162431"; 


#define FIREBASE_HOST "arduino-a0097-default-rtdb.firebaseio.com" 
#define FIREBASE_AUTH "0KB3qsganHG5gzRMmNcfTsI5809qLGr3nOcb4yRj" 

void setup() {
  Serial.begin(9600);
  nodemcu.begin(9600);
  while (!Serial) continue;
  Serial.print("Connecting");
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print(".");
    digitalWrite(ON_Board_LED, LOW);
    delay(250);
    digitalWrite(ON_Board_LED, HIGH);
    delay(250);
  }
  digitalWrite(ON_Board_LED, HIGH);
  Serial.println("");
  Serial.print("Successfully connected to : ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
  Serial.println();

  Firebase.begin(FIREBASE_HOST, FIREBASE_AUTH);
}
int id=0;
void loop() {

  StaticJsonBuffer<1000> jsonBuffer;
  JsonObject& data = jsonBuffer.parseObject(nodemcu);

  if (data == JsonObject::invalid()) {
    Serial.println("Invalid Json Object");
    jsonBuffer.clear();
    return;
  }
  Serial.println("JSON Object Recieved");
  id++;
  String idString=(String)id;
  String path = "/Data/id"+idString;
  
  String secondsString = data["seconds"];
  String centiseconds = data["centiseconds"];
  String fails = data["numberOfFails"];
  String currentHour=data["currentHour"];
  String currentDate=data["currentDate"];

  Firebase.setString(path+"/seconds", secondsString);
  Firebase.setString(path+"/centiseconds", centiseconds);
  Firebase.setString(path+"/currentHour", currentHour);
  Firebase.setString(path+"/numberOfFails", fails);
  Firebase.setString(path+"/currentDate", currentDate);
    
  if (Firebase.failed()) {
    Serial.print("setting /number failed:");
    Serial.println(Firebase.error());
    return;
  }
  delay(1000);
}
