#include <Wire.h>
#include <LiquidCrystal_I2C.h>
LiquidCrystal_I2C lcd(0x27, 16, 2);
#include <SoftwareSerial.h>
#include <ArduinoJson.h>
#include <DS3231.h>
DS3231  rtc(SDA, SCL);

SoftwareSerial nodemcu(5, 6);

#define NOTE_C4  262
#define NOTE_G3  196
#define NOTE_A3  220
#define NOTE_B3  247
#define NOTE_C4  262

int melody[] = {
  NOTE_C4, NOTE_G3, NOTE_G3, NOTE_A3, NOTE_G3, 0, NOTE_B3, NOTE_C4
};

int noteDurations[] = {
  4, 8, 8, 4, 4, 4, 4, 4
};

const int startPin = 10;
const int failPin = 9;
const int endPin = 8;
const int buzzerPin = 7;
long duration;
int ok = 1;
long  seconds;
long start_time;
long  centiseconds;
int count = 0;

enum GameState {FAILED, START, SUCCESS};
GameState gameState = GameState::FAILED;

void playWinMelody()
{
  for (int thisNote = 0; thisNote < 8; thisNote++) {
    int noteDuration = 1000 / noteDurations[thisNote];
    tone(buzzerPin, melody[thisNote], noteDuration);
    int pauseBetweenNotes = noteDuration * 1.60;
    delay(pauseBetweenNotes);
    noTone(buzzerPin);
  }
}

void setup(void)
{
  Serial.begin(9600);
  nodemcu.begin(9600);
  rtc.begin();
  lcd.begin();
  lcd.setCursor(0, 0);
  lcd.print(" Buzz Wire Game ");
  pinMode(buzzerPin, OUTPUT);
  pinMode(startPin, INPUT_PULLUP);
  pinMode(failPin, INPUT_PULLUP);
  pinMode(endPin, INPUT_PULLUP);
}

void loop(void)
{
  StaticJsonBuffer<1000> jsonBuffer;
  JsonObject& data = jsonBuffer.createObject();

  switch (gameState)
  {
    case GameState::START:
      if (!digitalRead(endPin)) {
        lcd.clear();
        lcd.print("CONGRATULATIONS!");
        playWinMelody();
        lcd.clear();
        gameState = GameState::SUCCESS;
        lcd.print("Time: ");
        lcd.print(seconds);
        lcd.print(" : ");
        lcd.print(centiseconds);
        lcd.setCursor(0, 1);
        lcd.print("Fails: ");
        lcd.print(count);
        delay(5000);
        lcd.clear();

        Serial.println(rtc.getTimeStr());
        Serial.println(rtc.getDateStr());
        data["seconds"] = seconds;
        data["centiseconds"] = centiseconds;
        data["currentHour"] = rtc.getTimeStr();
        data["currentDate"] = rtc.getDateStr();
        data["numberOfFails"] = count;
        Serial.print(seconds);
        Serial.print(centiseconds);
        data.printTo(nodemcu);
        jsonBuffer.clear();
        seconds = 0;
        centiseconds = 0;
        count = 0;
      }
      else if (!digitalRead(failPin)) {
        lcd.clear();
        lcd.print("FAILED");
        lcd.setCursor(0, 1);
        lcd.print("Time: ");
        lcd.print(seconds);
        lcd.print(" : ");
        lcd.print(centiseconds);
        tone(buzzerPin, 440, 200);
        delay(200);
        tone(buzzerPin, 415, 200);
        delay(200);
        tone(buzzerPin, 392, 200);
        delay(200);
        tone(buzzerPin, 370, 400);
        delay(1000);
        lcd.clear();
        gameState = GameState::FAILED;
        seconds = 0;
        centiseconds = 0;
      }
      break;
    case GameState::FAILED:
    case GameState::SUCCESS:
      if (!digitalRead(startPin)) {
        lcd.clear();
        gameState = GameState::START;
        lcd.print("NEW GAME");
        tone(buzzerPin, 440, 100);
        delay(120);
        tone(buzzerPin, 554, 100);
        delay(120);
        tone(buzzerPin, 659, 200);

        start_time = millis();
        while (ok)
        {
          lcd.setCursor(0, 1);
          seconds = (millis() - start_time) / 1000;
          centiseconds = ((millis() - start_time) / 10) % 100;
          lcd.print(seconds);
          lcd.print(" : ");
          lcd.print(centiseconds);
          if (!digitalRead(failPin))
          {
            count++;
            break;
          }
          else if (!digitalRead(endPin))
          {
            break;
          }
        }
        break;
      }
  }
}
