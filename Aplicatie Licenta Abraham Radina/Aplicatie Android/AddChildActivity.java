package com.example.arduino_app;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.arduino_app.fragments.RecyclerViewFragment;
import com.example.arduino_app.helpers.UtilsValidators;
import com.example.arduino_app.models.ChildData;
import com.example.arduino_app.models.DatabaseData;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class AddChildActivity extends AppCompatActivity {

    FirebaseDatabase database = FirebaseDatabase.getInstance("https://arduino-a0097-default-rtdb.firebaseio.com/");
    DatabaseReference myRef = database.getReference();
    String advisorID;
    String username;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_child);

        advisorID="id";
        Bundle extras=getIntent().getExtras();
        if(extras!=null)
        {
            advisorID=extras.getString("advisorID");
        }

        findViewById(R.id.btn_add).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
//                DatabaseReference myRef = database.getReference().child("Data");
//                myRef.removeValue();
                addChild();
            }
        });

        findViewById(R.id.btn_back).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                goToHome(advisorID);
            }
        });

    }

    private void goToHome(String advisorID) {
        Intent intent = new Intent(this, HomeActivity.class);
        intent.putExtra("advisorID",advisorID);
        startActivity(intent);

        this.finish();
    }

    private void goToSpinActivityAddChild(String advisorID,String id,String fullName) {
        Intent intent = new Intent(this, SpinActivityAddChild.class);
        intent.putExtra("advisorID",advisorID);
        intent.putExtra("id",id);
        intent.putExtra("fullName",fullName);
        startActivity(intent);

        this.finish();
    }

    private void addChild() {
        EditText fullNameEdt = findViewById(R.id.edt_fullName);
        EditText idEdt=findViewById(R.id.edt_id);
        String fullName = fullNameEdt.getText().toString().trim();
        username = idEdt.getText().toString().trim();

        if (fullName.matches("")) {
            fullNameEdt.setError("The full name field is empty!");
            return;
        }

        if (username.matches("")) {
            idEdt.setError("The username field is empty!");
            return;
        }

        DatabaseReference rootRef = FirebaseDatabase.getInstance().getReference().child(advisorID);
        rootRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                String idFinal=idEdt.getText().toString();
                if (snapshot.hasChild(idFinal)) {
                    idEdt.setError("Username already exists!");
                }
                else {
                    idEdt.setError(null);
                    Toast.makeText(AddChildActivity.this, "Child added", Toast.LENGTH_SHORT).show();
                    String fullName = fullNameEdt.getText().toString();

                    String id=idEdt.getText().toString();
                    writeNewData(fullName,id,advisorID);
                    goToSpinActivityAddChild(advisorID,id,fullName);
                    return;
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

    }

    public void writeNewData
            (String fullName,String id, String advisorID) {

        DatabaseReference ref=myRef.child(advisorID).child(id);

        ChildData data = new ChildData(fullName,id);
        ref.child("fullName").setValue(data.getFullName());
        ref.child("id").setValue(data.getId());
    }
}