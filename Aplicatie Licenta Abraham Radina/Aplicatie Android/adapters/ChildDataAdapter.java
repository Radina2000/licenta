package com.example.arduino_app.adapters;

import android.telecom.Call;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.arduino_app.R;
import com.example.arduino_app.interfaces.OnItemClick;
import com.example.arduino_app.models.ChildData;
import com.example.arduino_app.models.DatabaseData;

import java.util.ArrayList;

public class ChildDataAdapter extends RecyclerView.Adapter<ChildDataAdapter.LocationsHolder>
{
    private ArrayList<ChildData> children;
    private ItemClickListener itemClickListener;

    private OnItemClick onItemClick;

    public ChildDataAdapter(ArrayList<ChildData> locations, ItemClickListener itemClickListener) {
        this.children = locations;
        this.itemClickListener=itemClickListener;
    }

    @NonNull
    @Override
    public ChildDataAdapter.LocationsHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());


        View view = inflater.inflate(R.layout.child_item_cell, parent, false);
        LocationsHolder locationsViewHolder = new LocationsHolder(view);

        return locationsViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ChildDataAdapter.LocationsHolder holder, int position) {

        Animation animation= AnimationUtils.loadAnimation(holder.itemView.getContext(), android.R.anim.slide_in_left);
        ChildData locations = (ChildData) this.children.get(position);
        ((ChildDataAdapter.LocationsHolder) holder).bind(locations);

        holder.itemView.startAnimation(animation);
        holder.itemView.setOnClickListener(view->{itemClickListener.onItemClick(position);});
    }

    @Override
    public int getItemCount() {
        return this.children.size();
    }

    public interface ItemClickListener{
        void onItemClick(int position);
    }

    class LocationsHolder extends RecyclerView.ViewHolder {

        private TextView name;
        private TextView username;

        View view;

        public LocationsHolder(@NonNull View itemView) {
            super(itemView);

            name = itemView.findViewById(R.id.txt_name);
            username = itemView.findViewById(R.id.txt_username);
            this.view = itemView;

        }

        void bind(ChildData data) {

            name.setText(data.getFullName());
            username.setText(data.getId());
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onItemClick != null) {

                        notifyItemChanged(getAdapterPosition());
                    }
                }
            });

        }

    }
}

