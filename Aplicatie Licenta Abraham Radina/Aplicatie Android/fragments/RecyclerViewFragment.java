package com.example.arduino_app.fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.arduino_app.AddChildActivity;
import com.example.arduino_app.ChildActivity;
import com.example.arduino_app.ChildrenEvolutionActivity;
import com.example.arduino_app.R;
import com.example.arduino_app.adapters.ChildDataAdapter;
import com.example.arduino_app.interfaces.ActivitiesFragmentsCommunication;
import com.example.arduino_app.models.ChildData;
import com.example.arduino_app.models.DatabaseData;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class RecyclerViewFragment extends Fragment {

    public static final String TAG_RECYCLERVIEW_FRAGMENT = "TAG_RECYCLERVIEW_FRAGMENT";

    FirebaseDatabase database = FirebaseDatabase.getInstance("https://arduino-a0097-default-rtdb.firebaseio.com/");
    DatabaseReference myRef = database.getReference();
    ArrayList<ChildData> childrenList =new ArrayList<>();
    ArrayList<Float> scoreList =new ArrayList<>();
    ArrayList<Float> first3ScoreList =new ArrayList<>();
    ArrayList<Float> allScoreList =new ArrayList<>();
    ArrayList<String> nameList=new ArrayList<>();
    ArrayList<String> idList=new ArrayList<>();
    ArrayList<DatabaseData> databaseDataArrayList=new ArrayList<>();
    private Button btnRead;
    RecyclerView recyclerView;

    String advisorID;

    ChildDataAdapter childDataAdapter;
    public RecyclerViewFragment() {
    }

    public static RecyclerViewFragment newInstance() {

        Bundle args = new Bundle();
        RecyclerViewFragment fragment = new RecyclerViewFragment();
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_recycler_view, container, false);

        advisorID = getArguments().getString("advisorID");
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView = (RecyclerView) view.findViewById(R.id.database_data_list);
        recyclerView.setLayoutManager(linearLayoutManager);

        getDataFromDatabase(advisorID);

        childDataAdapter = new ChildDataAdapter(childrenList, new ChildDataAdapter.ItemClickListener() {
            @Override
            public void onItemClick(int position) {
                Intent intent = new Intent(getActivity(), ChildActivity.class);

                intent.putExtra("id",childrenList.get(position).getId());
                intent.putExtra("fullName",childrenList.get(position).getFullName());
                intent.putExtra("advisorID",advisorID);

                startActivity(intent);
                getActivity().finish();

            }
        });

        TextView myAwesomeTextView = view.findViewById(R.id.txt_popular_products);

        myAwesomeTextView.setText("Welcome "+advisorID);

        recyclerView.setAdapter(childDataAdapter);

        btnRead = view.findViewById(R.id.btn_Read_recycler);
        btnRead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToAddChildActivity();
            }
        });
        view.findViewById(R.id.btn_chartsAll).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                goToChartActivity();
            }
        });
        return view;
    }

    private void getScoreForChildren(ArrayList<ChildData> childrenList)
    {
        for (int i=0;i<childrenList.size();i++)
        {
            String name=childrenList.get(i).getId();
            nameList.add(name);
            DatabaseReference ref=myRef.child(advisorID).child(childrenList.get(i).getId()).child("plays");
            ref.addListenerForSingleValueEvent(new ValueEventListener()
            {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {

                    databaseDataArrayList.clear();
                    float lastGamesScore=0;
                    float firstGamesScore=0;
                    float allGamesScore=0;
                    float sumOfTime=0;
                    int number=0;
                    float firstGamesSumOfTime=0;
                    int firstGamesNumber=0;
                    float allSumOfTime=0;
                    int allNumber=0;
                    int countIntern=0;
                    for (DataSnapshot ds : snapshot.getChildren()) {
                        DatabaseData database = ds.getValue(DatabaseData.class);

                        float centiseconds = (float) Float.parseFloat(database.getCentiseconds());
                        int seconds = (int) Integer.parseInt(database.getSeconds());
                        float finalCenti = centiseconds / 100;
                        float result = seconds + finalCenti;
                        String s = "" + result;
                        float value = (float) Float.parseFloat(s);

                        int numberOfFails = Integer.parseInt(database.getNumberOfFails());

                        allSumOfTime+=value;
                        allNumber+=numberOfFails;

                        if(countIntern<3)
                        {
                            firstGamesSumOfTime+=value;
                            firstGamesNumber+=numberOfFails;
                        }
                        else
                        {
                            sumOfTime+=value;
                            number+=numberOfFails;
                        }
                        countIntern++;
                    }
                    int n=countIntern;
                    if(number==0)
                        number=1;
                    if(sumOfTime==0)
                        lastGamesScore=0;
                    else {

                            lastGamesScore = ((100*countIntern) - sumOfTime)/number;
                    }

                    if(firstGamesNumber==0)
                        firstGamesNumber=1;
                    if(firstGamesSumOfTime==0)
                        firstGamesScore=0;
                    else {
                            firstGamesScore = ((100*countIntern) - firstGamesSumOfTime) / firstGamesNumber;
                    }

                    if(allNumber==0)
                        allNumber=1;
                    if(allSumOfTime==0)
                        allGamesScore=0;
                    else
                    {
                        allGamesScore=((100*countIntern)-allSumOfTime)/allNumber;
                    }

                    scoreList.add(lastGamesScore);
                    first3ScoreList.add(firstGamesScore);
                    allScoreList.add(allGamesScore);
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {
                }
            });

        }
    }

    private void getDataFromDatabase(String advisorID) {

        childrenList.clear();
        DatabaseReference reference = myRef.child(advisorID);
        reference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                childrenList.clear();
                for (DataSnapshot ds : snapshot.getChildren()) {
                    ChildData database = ds.getValue(ChildData.class);
                    childrenList.add(database);
                    idList.add(database.getId());
                }
                getScoreForChildren(childrenList);
                childDataAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
            }
        });
    }

    private void goToChartActivity() {
        Intent intent = new Intent(getActivity(), ChildrenEvolutionActivity.class);
        intent.putExtra("advisorID",advisorID);
        intent.putExtra("list",scoreList);
        intent.putExtra("3list",first3ScoreList);
        intent.putExtra("allList",allScoreList);
        intent.putExtra("nameList",nameList);
        intent.putExtra("idList",idList);

        startActivity(intent);

        getActivity().finish();
    }

    private void goToAddChildActivity() {
        Intent intent = new Intent(getActivity(), AddChildActivity.class);
        intent.putExtra("advisorID",advisorID);

        startActivity(intent);

        getActivity().finish();
    }

}