package com.example.arduino_app.models;

import java.util.ArrayList;
import java.util.List;

public class ChildData {
    String fullName;
    String id;

    public ChildData() {
    }

    public ChildData(String fullName, String id) {
        this.fullName = fullName;
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
