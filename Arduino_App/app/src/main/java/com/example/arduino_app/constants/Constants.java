package com.example.arduino_app.constants;

public class Constants {
    public static String BASE_URL = "https://my-json-server.typicode.com";
    public static String TITLE = "title";
    public static String ID = "id";
    public static String DESCRIPTION = "descriere";
    public static String URL = "url";
    public static String NAME = "nume";
}
