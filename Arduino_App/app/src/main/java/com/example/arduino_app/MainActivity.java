package com.example.arduino_app;

import static com.example.arduino_app.fragments.Welcome.TAG_WELCOME;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;

import com.example.arduino_app.fragments.ForgotPassword;
import com.example.arduino_app.fragments.Login;
import com.example.arduino_app.fragments.Register;
import com.example.arduino_app.R;
import com.example.arduino_app.fragments.Welcome;
import com.example.arduino_app.interfaces.ActivitiesFragmentsCommunication;

public class MainActivity extends AppCompatActivity implements ActivitiesFragmentsCommunication {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null) {
            onAddWelcomeFragment();
        }
    }

    private void onAddWelcomeFragment() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.frame_layout, Welcome.newInstance(), TAG_WELCOME);

        fragmentTransaction.commit();
    }

    @Override
    public void onReplaceFragment(String TAG) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment fragment;

        switch (TAG) {
            case Login.TAG_LOGIN: {
                fragment = Login.newInstance();
                break;
            }
            case Register.TAG_REGISTER: {
                fragment = Register.newInstance();
                break;
            }
            case ForgotPassword.TAG_FORGOT_PASSWORD: {
                fragment = ForgotPassword.newInstance();
                break;
            }
            case TAG_WELCOME: {
                fragment = Welcome.newInstance();
                break;
            }
            default:
                return;
        }
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frame_layout, fragment, TAG);

        fragmentTransaction.commit();
    }

    @Override
    public void addHomePageFragment() {

    }
}