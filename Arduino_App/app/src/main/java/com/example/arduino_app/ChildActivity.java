package com.example.arduino_app;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.example.arduino_app.adapters.DatabaseDataAdapter;
import com.example.arduino_app.fragments.RecyclerViewFragment;
import com.example.arduino_app.models.DatabaseData;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class ChildActivity extends AppCompatActivity {

    FirebaseDatabase database = FirebaseDatabase.getInstance("https://arduino-a0097-default-rtdb.firebaseio.com/");
    DatabaseReference myRef = database.getReference();
    ArrayList<DatabaseData> databaseDataArrayList = new ArrayList<DatabaseData>();

    RecyclerView recyclerView;

    DatabaseDataAdapter databaseDataAdapter;
    String id;
    String fullName;
    String advisorID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_child);

        TextView textView=findViewById(R.id.text_child);
        TextView textViewUsername=findViewById(R.id.text_username);

        id= "id";
        fullName= "id";
        Bundle extras=getIntent().getExtras();
        if(extras!=null)
        {
            id=extras.getString("id");
            fullName=extras.getString("fullName");
            advisorID=extras.getString("advisorID");
        }
        textView.setText("Name: "+fullName);
        textViewUsername.setText("Nickname: "+id);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView = (RecyclerView) findViewById(R.id.database_data_list);
        recyclerView.setLayoutManager(linearLayoutManager);
        getDataFromDatabase(id);

        databaseDataAdapter = new DatabaseDataAdapter(databaseDataArrayList);
        recyclerView.setAdapter(databaseDataAdapter);

        findViewById(R.id.btn_charts).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                goToChartActivity();
            }
        });

        findViewById(R.id.btn_StartSession).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                DatabaseReference myRef = database.getReference().child("Data");
                myRef.removeValue();
                goToSpinActivity(id);
            }
        });

        findViewById(R.id.btn_delete).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                DatabaseReference reference = myRef.child(advisorID).child(id);
                reference.removeValue();
                goToHomeActivity();
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.right_menu_evolution, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch(item.getItemId()) {
            case R.id.log_out:
                FirebaseAuth.getInstance().signOut();
                goToWelcome();
                return true;
            case R.id.page_home:
                goToHomeActivity();
                return true;
            default: return  super.onOptionsItemSelected(item);
        }
    }

    private void goToWelcome() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }
    private void goToHomeActivity()
    {
        Intent intent = new Intent(this, HomeActivity.class);
        intent.putExtra("advisorID",advisorID);
        startActivity(intent);
        finish();
    }

    private void goToChartActivity() {
        Intent intent = new Intent(this, ChartActivity.class);
        intent.putExtra("id",id);
        intent.putExtra("fullName",fullName);
        intent.putExtra("advisorID",advisorID);

        startActivity(intent);

        this.finish();
    }

    private void goToSpinActivity(String id) {
        Intent intent = new Intent(this, SpinActivity.class);
        intent.putExtra("id",id);
        intent.putExtra("fullName",fullName);
        intent.putExtra("advisorID",advisorID);
        startActivity(intent);

        this.finish();
    }

    public void getDataFromDatabase(String child) {
        databaseDataArrayList.clear();
        DatabaseReference reference = myRef.child(advisorID).child(child).child("plays");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                databaseDataArrayList.clear();
                ArrayList<String> id = new ArrayList<>();
                for (DataSnapshot ds : snapshot.getChildren()) {
                    DatabaseData database = ds.getValue(DatabaseData.class);
                    databaseDataArrayList.add(database);
                }
                databaseDataAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
            }
        });
    }

}