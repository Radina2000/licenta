package com.example.arduino_app.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.example.arduino_app.AddChildActivity;
import com.example.arduino_app.ChildActivity;
import com.example.arduino_app.helpers.UtilsValidators;
import com.example.arduino_app.HomeActivity;
import com.example.arduino_app.R;
import com.example.arduino_app.interfaces.ActivitiesFragmentsCommunication;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


public class Register extends Fragment {

    public static final String TAG_REGISTER = "TAG_REGISTER";
    private ActivitiesFragmentsCommunication fragmentCommunication;
    private FirebaseAuth auth;
    String advisorID;

    public static Register newInstance() {

        Bundle args = new Bundle();

        Register fragment = new Register();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        auth=FirebaseAuth.getInstance();
    }

    @Override
    public void onStart() {
        super.onStart();

        FirebaseUser currentUser=auth.getCurrentUser();
        if(currentUser!=null)
        {

        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_register, container, false);


    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof ActivitiesFragmentsCommunication) {
            fragmentCommunication = (ActivitiesFragmentsCommunication) context;
        }

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        view.findViewById(R.id.btn_register).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {


                validateEmailAndPassword();

            }
        });
        view.findViewById(R.id.btn_back).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                goToWelcome();
            }
        });

    }
    private void goToHome()
 {
        Intent intent = new Intent(getActivity(), HomeActivity.class);
        intent.putExtra("advisorID",advisorID);
        startActivity(intent);
        getActivity().finish();
    }

    private void goToWelcome() {

        if (fragmentCommunication != null) {
            fragmentCommunication.onReplaceFragment(Welcome.TAG_WELCOME);
        }
    }

    private void validateEmailAndPassword(){

        if(getView()==null)
        {
            return;
        }

        EditText username=getView().findViewById(R.id.edt_SecondName);
        EditText emailEdtText=getView().findViewById(R.id.edt_email);
        EditText passwordEdtText=getView().findViewById(R.id.edt_password);

        String email=emailEdtText.getText().toString();
        String password=passwordEdtText.getText().toString();
        advisorID=username.getText().toString();

        if(!UtilsValidators.isValidEmail(email)){
            emailEdtText.setError("Invalid Email");
            return;
        }
        else
        {
            emailEdtText.setError(null);
        }
        if(!UtilsValidators.isValidPassword(password)){
            passwordEdtText.setError("Invalid Password");
            return;
        }
        else
        {
            passwordEdtText.setError(null);
        }

        DatabaseReference rootRef = FirebaseDatabase.getInstance().getReference();
        rootRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                String idFinal=username.getText().toString();
                if (snapshot.hasChild(idFinal)) {
                    username.setError("Username already exists!");
                }
                if (advisorID.matches("")) {
                    username.setError("The username field is empty!");
                    return;
                }

                else {
                    username.setError(null);
                    createFirebaseUser(email,password);
                    return;
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });


    }
    private void createFirebaseUser(String email , String password){

        if(getActivity()==null)
        {
            return;
        }

        auth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful())
                        {
                            goToHome();
                            Toast.makeText(getContext(),"Authentication success",Toast.LENGTH_SHORT).show();
                        }else
                        {
                            Toast.makeText(getContext(),"Authentication failed",Toast.LENGTH_SHORT).show();
                        }
                    }
                });

    }
}