package com.example.arduino_app;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.example.arduino_app.adapters.DatabaseDataAdapter;
import com.example.arduino_app.models.DatabaseData;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class SpinActivity extends AppCompatActivity {

    FirebaseDatabase database = FirebaseDatabase.getInstance("https://arduino-a0097-default-rtdb.firebaseio.com/");
    DatabaseReference myRef = database.getReference();

    String id;
    String fullName;
    String advisorID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spin);

        id= "id";
        fullName= "id";
        advisorID="id";
        Bundle extras=getIntent().getExtras();
        if(extras!=null)
        {
            id=extras.getString("id");
            fullName=extras.getString("fullName");
            advisorID=extras.getString("advisorID");
        }

        findViewById(R.id.btn_endSession).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                writeNewData(id);
                goToChildActivity();
            }
        });
    }

    private void goToChildActivity() {
        Intent intent = new Intent(this, ChildActivity.class);
        intent.putExtra("id",id);
        intent.putExtra("fullName",fullName);
        intent.putExtra("advisorID",advisorID);
        startActivity(intent);

        this.finish();
    }


    public void writeNewData
            (String id) {
        DatabaseReference reference = database.getReference().child("Data");

        DatabaseReference ref=myRef.child(advisorID).child(id);
        reference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for (DataSnapshot ds : snapshot.getChildren()) {

                    DatabaseReference playRef=ref.child("plays");
                    DatabaseData databaseData = ds.getValue(DatabaseData.class);
                    if(databaseData.getSeconds()==null)
                        databaseData.setSeconds("5");
                    if(databaseData.getCentiseconds()==null)
                        databaseData.setCentiseconds("10");
                    if(databaseData.getNumberOfFails()==null)
                        databaseData.setNumberOfFails("1");
                    String currentDate = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault()).format(new Date());
                    String currentTime = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(new Date());
                    if(databaseData.getCurrentDate()==null)
                        databaseData.setCurrentDate(currentDate);
                    if(databaseData.getCurrentHour()==null)
                        databaseData.setCurrentHour(currentTime);
                    playRef.push().setValue(databaseData);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

    }


}