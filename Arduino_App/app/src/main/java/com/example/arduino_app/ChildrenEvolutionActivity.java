package com.example.arduino_app;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.example.arduino_app.adapters.ChildDataAdapter;
import com.example.arduino_app.adapters.DatabaseDataAdapter;
import com.example.arduino_app.models.ChildData;
import com.example.arduino_app.models.DatabaseData;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class ChildrenEvolutionActivity extends AppCompatActivity {

    String id;
    String fullName;
    String advisorID;
    BarChart barChart;
    LineChart lineChart;

    ArrayList<Float> scorelist = new ArrayList<>();
    ArrayList<String> nameList=new ArrayList<>();
    ArrayList<Float> first3Scorelist = new ArrayList<>();
    ArrayList<Float> allList = new ArrayList<>();
    ArrayList<String> idList =new ArrayList<>();
    ArrayList<DatabaseData> databaseDataArrayList = new ArrayList<DatabaseData>();
    FirebaseDatabase database = FirebaseDatabase.getInstance("https://arduino-a0097-default-rtdb.firebaseio.com/");
    DatabaseReference myRef = database.getReference();
    DatabaseDataAdapter databaseDataAdapter;
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.right_menu_evolution, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch(item.getItemId()) {
            case R.id.log_out:
                FirebaseAuth.getInstance().signOut();
                goToWelcome();
                return true;
            case R.id.page_home:
                goToRecyclerViewFragment();
                return true;
            default: return  super.onOptionsItemSelected(item);
        }
    }

    private void goToRecyclerViewFragment() {
        Intent intent = new Intent(this, HomeActivity.class);
        intent.putExtra("advisorID",advisorID);
        startActivity(intent);
        finish();
    }

    private void goToWelcome() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_children_evolution);

        id= "id";
        fullName= "id";
        advisorID= "id";
        Bundle extras=getIntent().getExtras();
        if(extras!=null)
        {
            id=extras.getString("id");
            fullName=extras.getString("fullName");
            advisorID=extras.getString("advisorID");
            scorelist= (ArrayList<Float>) getIntent().getSerializableExtra("list");
            first3Scorelist= (ArrayList<Float>) getIntent().getSerializableExtra("3list");
            nameList=(ArrayList<String>) getIntent().getSerializableExtra("nameList");
            allList=(ArrayList<Float>) getIntent().getSerializableExtra("allList");
            idList = (ArrayList<String>) getIntent().getSerializableExtra("idList");
        }

        getScoreForChildren();

        barChart = findViewById(R.id.lineChart);
        lineChart = findViewById(R.id.pieChart);
        showBarChart();
        showLineChart();
    }
    private void getScoreForChildren()
    {
        for (int i=0;i<idList.size();i++)
        {
            databaseDataArrayList.clear();
            DatabaseReference reference = myRef.child(advisorID).child(idList.get(i)).child("plays");
            reference.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    databaseDataArrayList.clear();
                    ArrayList<String> id = new ArrayList<>();
                    for (DataSnapshot ds : snapshot.getChildren()) {
                        DatabaseData database = ds.getValue(DatabaseData.class);
                        databaseDataArrayList.add(database);
                    }

                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {
                }
            });
        }
    }

    private void showLineChart(){

        ArrayList<Entry> lineEntries = new ArrayList<>();

        for(int i=0;i<allList.size();i++)
        {
            Entry barEntry = new Entry(i, allList.get(i));
            lineEntries.add(barEntry);
        }

        ArrayList<Entry> lineEntriesGoal = new ArrayList<>();

        for(int i=0;i<first3Scorelist.size();i++)
        {
            float percentage=(50*first3Scorelist.get(i))/100;
            float value=first3Scorelist.get(i)+percentage;
            Entry barEntry = new Entry(i, value);
            lineEntriesGoal.add(barEntry);
        }

        LineDataSet lineDataSet = new LineDataSet(lineEntries, "Score");
        lineDataSet.setColors(Color.DKGRAY);
        LineDataSet lineDataSet1 = new LineDataSet(lineEntriesGoal, "Goal");
        lineDataSet1.setColors(Color.MAGENTA);
        LineData barData = new LineData(lineDataSet,lineDataSet1);
        lineChart.setData(barData);
        if(allList.size()>=6) {
            lineChart.getXAxis().setValueFormatter(new IndexAxisValueFormatter(nameList));
            lineChart.getXAxis().setTextColor(Color.MAGENTA);
            lineChart.getXAxis().setTextSize(11);
            lineChart.setVisibleXRangeMaximum(6);
        }
        lineChart.getDescription().setText("Goal = First Games Score + 50%");
        lineChart.animateY(5000);
        lineChart.invalidate();

    }


    private void showBarChart() {
        ArrayList<BarEntry> entries = new ArrayList<>();
        ArrayList<BarEntry> firstEntries = new ArrayList<>();
        for(int i=0;i<scorelist.size();i++)
        {
            BarEntry barEntry = new BarEntry(i, scorelist.get(i));
            entries.add(barEntry);
        }
        for(int i=0;i<first3Scorelist.size();i++)

        {
            BarEntry barEntry = new BarEntry(i, first3Scorelist.get(i));
            firstEntries.add(barEntry);
        }
        BarDataSet barDataSet2 = new BarDataSet(entries, "Last Games Score");
        barDataSet2.setColors(Color.MAGENTA);
        BarDataSet barDataSet1 = new BarDataSet(firstEntries, "First Games Score");
        barDataSet1.setColors(Color.CYAN);

        BarData barData = new BarData(barDataSet1,barDataSet2);
        barChart.setData(barData);
        if(allList.size()>=7) {
            barChart.getXAxis().setValueFormatter(new IndexAxisValueFormatter(nameList));
            barChart.getXAxis().setTextColor(Color.MAGENTA);
            barChart.getXAxis().setTextSize(10);
            barChart.getXAxis().setCenterAxisLabels(true);
            barChart.setVisibleXRangeMaximum(7);
            barData.setBarWidth(0.3f);
            float groupSpace = 0.4f;
            barChart.animateY(3000);
            barChart.animate();
            barChart.groupBars(0, groupSpace, 0);
        }
        else
        {
            barData.setBarWidth(0.3f);
            float groupSpace = 0.4f;
            barChart.animateY(3000);
            barChart.animate();
            barChart.groupBars(-0.5f, groupSpace, 0);
        }

        barChart.getDescription().setEnabled(false);
        barChart.invalidate();
    }
}