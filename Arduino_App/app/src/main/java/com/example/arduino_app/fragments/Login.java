package com.example.arduino_app.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.arduino_app.SpinActivity;
import com.example.arduino_app.helpers.UtilsValidators;
import com.example.arduino_app.BuildConfig;
import com.example.arduino_app.HomeActivity;
import com.example.arduino_app.R;
import com.example.arduino_app.interfaces.ActivitiesFragmentsCommunication;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


public class Login extends Fragment {

    public static final String TAG_LOGIN = "TAG_LOGIN";

    private ActivitiesFragmentsCommunication fragmentCommunication;
    private FirebaseAuth auth;

    public static Login newInstance() {

        Bundle args = new Bundle();

        Login fragment = new Login();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        auth = FirebaseAuth.getInstance();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_login, container, false);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        if (context instanceof ActivitiesFragmentsCommunication) {
            fragmentCommunication = (ActivitiesFragmentsCommunication) context;
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (BuildConfig.DEBUG) {
            EditText editTextEmail = view.findViewById(R.id.edt_email);
            editTextEmail.setText("abrahamradina2000@gmail.com");

            EditText editTextPassword = view.findViewById(R.id.edt_password);
            editTextPassword.setText("Test1234");

            EditText advisorIDTextPassword = view.findViewById(R.id.edt_id);
            advisorIDTextPassword.setText("Radina2000");
        }

        view.findViewById(R.id.btn_login).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                validateEmailAndPassword();
            }
        });

        view.findViewById(R.id.btn_register).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                goToRegister();

            }
        });
        view.findViewById(R.id.btn_forgotPassword).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                goToForgotPassword();
            }
        });
    }

    private void goToHome(String advisorID) {
        Intent intent = new Intent(getActivity(), HomeActivity.class);
        intent.putExtra("advisorID",advisorID);
        startActivity(intent);

        getActivity().finish();
    }

    private void goToRegister() {

        if (fragmentCommunication != null) {
            fragmentCommunication.onReplaceFragment(Register.TAG_REGISTER);
        }
    }

    private void goToForgotPassword() {

        if (fragmentCommunication != null) {
            fragmentCommunication.onReplaceFragment(ForgotPassword.TAG_FORGOT_PASSWORD);
        }
    }

    private void validateEmailAndPassword() {

        if (getView() == null) {
            return;
        }
        EditText emailEdtText = getView().findViewById(R.id.edt_email);
        EditText passwordEdtText = getView().findViewById(R.id.edt_password);
        EditText advisorEdtText=getView().findViewById(R.id.edt_id);

        String email = emailEdtText.getText().toString().trim();

        String password = passwordEdtText.getText().toString();

        String advisorID=advisorEdtText.getText().toString();

        if (!UtilsValidators.isValidEmail(email)) {
            emailEdtText.setError("Invalid Email");
            return;
        } else {
            emailEdtText.setError(null);
        }
        if (!UtilsValidators.isValidPassword(password)) {
            passwordEdtText.setError("Invalid Password");
            return;
        } else
        {
            passwordEdtText.setError(null);
        }
        DatabaseReference rootRef = FirebaseDatabase.getInstance().getReference();
        rootRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                if (snapshot.hasChild(advisorID) && !advisorID.matches("")) {
                    loginFirebaseUser(email, password,advisorID);
                }
                else {
                    if (advisorID.matches("")) {
                        advisorEdtText.setError("The username field is empty!");
                        return;
                    }
                    advisorEdtText.setError("Username not found!");
                    return;
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    private void loginFirebaseUser(String email, String password,String advisorID) {

        auth.signInWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {

                    goToHome(advisorID);
                    Toast.makeText(getContext(), "Authentication success", Toast.LENGTH_SHORT).show();

                } else {

                    Toast.makeText(getContext(), "Authentication failed", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

}